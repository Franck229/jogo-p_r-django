import './App.css';
import React, { Component } from 'react';
import QuestionComponent from './components/home.js';
import AddPergunta from './components/addPergunta.js';
import Home from './components/navbar.js'


class App extends Component {

  render(){
    return (
      <Home onchange={this.handleChange} onsubmit={this.handleSubmit} value={this.state.value} dados={this.state.perguntas}></Home>
      //<QuestionComponent perguntas={this.state.perguntas}></QuestionComponent>
    )
  }

  constructor(props){
    super(props);

    this.state = {
      perguntas: [],
      value: {
        enunciado: "a",
        alternativa0: "",
        alternativa1: "",
        alternativa: "",
        alternativa3: "",
        Rcorreta: "",
      }
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    alert('Uma dissertação foi enviada: ' + this.state.value.enunciado);
    event.preventDefault();
  }

  componentDidMount(){
    const apiUrl = 'http://localhost:8000/perguntas/';
    fetch(apiUrl)
        .then((response) => response.json())
        .then((data) => {
        this.setState({perguntas: data})    
      });
  }

}
export default App;
