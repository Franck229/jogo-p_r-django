import React, { Component } from 'react'
import './style.css';

class MyForm extends React.Component {
    constructor() {
      super();
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleSubmit(event) {
      event.preventDefault();
      const data = new FormData(event.target);
      fetch('http://localhost:8000/perguntas/',{
        method: 'POST',
        body: data,
      }).then((r)=> {if(r.status == 201){
        alert('Pergunta criada com sucesso!')
      }else{
          alert('Erro ao criar pergunta tente novamente')
      }});
      
    }
  
    render() {
      return (
        <form onSubmit={this.handleSubmit}>
          <label >Enunciado</label>
          <input name="enunciado" id="Enunciado"  type="text" />
  
          <label >Alternativa 1</label>
          <input name="alternativa0" id="a1"  type="text" />
  
          <label >Alternativa 2</label>
          <input name="alternativa1" id="a2"  type="text" />

          <label >Alternativa 3</label>
          <input name="alternativa2" id="a3"  type="text" />
  
          <label >Alternativa 4</label>
          <input name="alternativa3" id="a4"  type="text" />
  
          <label >Indice da Resposta Correta</label>
          <input name="Rcorreta" id="rc"  type="text" />
  
          <button className="Link" to="/">Adicionar</button>
        </form>
      );
    }
  }

/* 

function AddPergunta(props)  {


        const [data, setData] = React.useState(props.value);
        const form = React.useRef(null)
      
        function submit(){
            const data = new FormData(form.current);
            console.log(data)
            alert(data.enunciado)
        }

          


    return(
        <div>
            <form ref={form} onSubmit={submit}>
                <label>
                    <span>Enunciado</span>
                    <input defaultValue={data.enunciado} type="text"/>
                </label>
                <label>
                    <span>Alternativa 1</span>
                    <input defaultValue={data.alternativa0} type="text"/>
                </label>
                <label>
                    <span>Alternativa 2</span>
                    <input defaultValue={data.alternativa1} type="text"/>
                </label>
                <label>
                    <span>Alternativa 3</span>
                    <input defaultValue={data.alternativa2} type="text"/>
                </label>
                <label>
                    <span>Alternativa 4</span>
                    <input defaultValue={data.alternativa3} type="text"/>
                </label>
                <label>
                    <span>Indice da Resposta Correta</span>
                    <input defaultValue={data.Rcorreta} type="text"/>
                </label>
                    <input type="submit" value="Enviar"/>                
            </form>
            
            
        </div>
    )
}*/

export default MyForm