import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import QuestionComponent from './home.js';
import AddPergunta from './addPergunta.js';
import './style.css';


// This site has 3 pages, all of which are rendered
// dynamically in the browser (not server rendered).
//
// Although the page does not ever refresh, notice how
// React Router keeps the URL up to date as you navigate
// through the site. This preserves the browser history,
// making sure things like the back button and bookmarks
// work properly.

export default function Home(props) {

    const [showBTN, setBTN] = React.useState(false);
    
  return (
    <Router>
      <div>
          <center>
            <ul>
            <li>
                <Link className="Link "to="/">Jogar</Link>
            </li>
            <li>aaaa</li>
            <li>a</li>
            <li></li>
            <li>
                {showBTN ? <Link className="Link " to="/addPergunta">Adicionar Pergunta</Link> : null}
            </li>
            </ul>
        </center>
        <hr />

        {/*
          A <Switch> looks through all its children <Route>
          elements and renders the first one whose path
          matches the current URL. Use a <Switch> any time
          you have multiple routes, but you want only one
          of them to render at a time
        */}

        
        <Switch>
          <Route exact path="/">
            <QuestionComponent perguntas={props.dados} showBTN={setBTN}/>
          </Route>
          <Route path="/addPergunta">
            <AddPergunta onchange={props.onchange} onsubmit={props.onsubmit} value={props.value} />
          </Route>
        </Switch>
      </div>
    </Router>
  );
  
}
