import React from 'react';
import './style.css'

    var count = 0;

    function QuestionComponent ({ perguntas,showBTN }) {
      const [list, changeList] = React.useState([0, 1]);
      const [acertos, acertou] = React.useState(0);
      const [showBTNPular, setBTNPular] = React.useState(true);
      ;

      function pular(){
        if(perguntas.length == list[1]){
          alert('As perguntas acabaram, voce acertou ' + acertos   +'/'+perguntas.length+' ,volte para o começo')
          count=0;
          acertou(0);
          changeList([0,1])
          setBTNPular(true);
          return (
            <div>
              <center><h1>Jogo de perguntas e respostas</h1></center>
                <center>
                  <div>
                    <div>
                      <h1>Pontuação: {acertos}</h1>
                      <button  type="button" onClick={()=> (acertou(0),changeList([0,1]), setBTNPular(true), count=0)}>Jogar novamente</button>
                    </div>
                  </div>
                </center>
            </div>
          );
        }else{
          setBTNPular(false);
        }
        changeList([list[0]+1, list[1]+1]);
        count++;
      }

      function estacerta(index, respostaCerta){
        count = count + 1;
        if(index == respostaCerta){
          acertou(acertos + 1);
          if(perguntas.length == list[1]){
            if(acertos+1 == perguntas.length){
              alert('Parabens você acertou todas as perguntas!')
              alert('Agora você pode adicionar uma nova pergunta!')
              showBTN(true);
            }else{
              alert('As perguntas acabaram, voce acertou ' + acertos + 1   +'/'+perguntas.length)
              alert('Que pena você errou! volte do começo')
              acertou(0)
              changeList([0,1])
            }
            
          }else{
            alert('Parabéns você acertou! proxima pergunta!')
            changeList([list[0]+1, list[1]+1])
          }
          
        }else{
          alert('Que pena você errou! volte do começo')
          acertou(0);
          changeList([0,1])
        }
      }
      console.log(count)
      if(count == perguntas.length){
        return (
          <div>
            <center><h1>Jogo de perguntas e respostas</h1></center>
              <center>
                <div>
                  <div>
                    <h1>Pontuação: {acertos}</h1>
                    <button className="Link" type="button" onClick={()=> (acertou(0),changeList([0,1]), setBTNPular(true), count=0)}>Jogar novamente</button>
                  </div>
                </div>
              </center>
          </div>
        );
      }else{

        return (
          <div>
            <center><h1>Jogo de perguntas e respostas</h1></center>
            {perguntas.slice(list[0], list[1]).map((perguntas, i) => (
              <center>
                <div>
                  <div className="multi-button">
                    <h1 id={i}>{perguntas.enunciado}</h1>
                    <button onClick={() => estacerta(1, perguntas.Rcorreta)}>{perguntas.alternativa0}</button>
                    <button onClick={() => estacerta(2, perguntas.Rcorreta)}>{perguntas.alternativa1}</button>
                    <button onClick={() => estacerta(3, perguntas.Rcorreta)}>{perguntas.alternativa2}</button>
                    <button onClick={() => estacerta(4, perguntas.Rcorreta)}>{perguntas.alternativa3}</button>
                    <h1>Pontuação: {acertos}</h1> 
                    {showBTNPular ? <button onClick={pular}>Pular Pergunta</button> : null}
                  </div>
                </div>
              </center>
            ))}
          </div>
        );
      }
      
    };
  

  


export default QuestionComponent 