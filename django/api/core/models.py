from django.db import models

class Pergunta(models.Model):
    enunciado = models.TextField(max_length=200)
    Rcorreta = models.IntegerField()
    alternativa0 = models.TextField(max_length=100)
    alternativa1 = models.TextField(max_length=100)
    alternativa2 = models.TextField(max_length=100)
    alternativa3 = models.TextField(max_length=100)

    def __str__(self):
        return self.enunciado