from django.shortcuts import render
from rest_framework import viewsets
from .models import Pergunta
from .serializer import PerguntaSerializer


class PerguntaViewSet(viewsets.ModelViewSet):
    queryset = Pergunta.objects.all()
    serializer_class = PerguntaSerializer
