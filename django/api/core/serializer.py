from rest_framework import serializers
from .models import Pergunta 

class PerguntaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pergunta
        fields = ['enunciado', 'Rcorreta', 'alternativa0','alternativa1','alternativa2','alternativa3']
