# README #
JOGO DE PERGUNTAS E RESPOSTAS 

### What is this repository for? ###
Trata-se de um jogo de Perguntas e Respostas cujo o api é criado em python utilizando Django como framework que 
serve dados para uma aplicação frontend em javascript implementada utilizando
React JS.


### How do I get set up? ###

* abra seu terminal e digite:
* sudo apt install git
* git clone git@bitbucket.org:Franck229/jogo-p_r-django.git 
* cd jogo-p_r-django/
* Agora vamos primeiro rodar a API
* cd django/api 
* ativar a virtual env
* . ../venv/bin/activate 
*  Agora vamos rodar a API 
*  python manage.py runserver 0.0.0.0:8000

se tudo ocorreu bem no seu terminal deve ter algo mais ou menos assim:
Watching for file changes with StatReloader Performing system checks...
System check identified no issues (0 silenced). December 10, 2020 - 12:46:02
Django version 3.1.4, using settings 'api.settings' Starting development server at http://0.0.0.0:8000/ Quit the server with CONTROL-C
* Agora vamos iniciar a aplicação, abra outro terminal(ctrl+shift+t) e volte ao diretório inicial
* cd ../../
* entremos no frontend e então na aplicação
* cd frontend/jogo-perguntas
* vamos instalar agora os packages
* npm install
* Agora vamos iniciar a aplicação
* npm start

### Who do I talk to? ###

* Franck kumako
